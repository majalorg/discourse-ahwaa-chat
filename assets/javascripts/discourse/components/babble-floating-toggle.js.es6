export default Ember.Component.extend({
  babbleReady: false,
  babbleVisible: false,
  babbleHasUnread: false,
  classNameBindings: ['className'],
  className: Ember.computed('babbleVisible', 'babbleHasUnread', function() {
    const babbleVisible = this.get('babbleVisible');
    const babbleHasUnread = this.get('babbleHasUnread');

    return !babbleVisible && babbleHasUnread ? 'babble-unread' : '';
  }),

  init() {
    this._super();

    this.babbleEvents();
  },

  babbleEvents() {
    this.appEvents.on('babble-default-registered', () => {
      this.set('babbleReady', true);
    });

    this.appEvents.on('babble-update-visible', babbleVisible => {
      this.set('babbleVisible', babbleVisible);
    });

    this.appEvents.on('babble-update-unread', isUnread => {
      this.set('babbleHasUnread', isUnread);
    });
  },

  click() {
    this.appEvents.trigger('babble-toggle-chat');
  },
});
